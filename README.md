Use this repository to save all FileNet solutions related artifacts. Create one folder for each applications and at least save following artifacts:
1- FileNet Deployment Manager package
2- Principal Label file
3- Desktop export (properties file)
4 - Instruction set for manual steps
* Other community or team contact